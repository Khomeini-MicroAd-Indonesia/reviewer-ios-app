//
//  SpecificDetailViewController.swift
//  reviewerApp
//
//  Created by Khomeini . on 7/7/16.
//  Copyright © 2016 Khomeini . All rights reserved.
//

import UIKit

class SpecificDetailViewController: UIViewController{
    
    // MARK: - Properties
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var ratingControl: RatingControl!
    @IBOutlet weak var subjectLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    
    var travel: Travel?
    var food: Food?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       
        if let travel = travel {
            
            photoImageView.image = travel.photo
            ratingControl.rating = travel.rating
            subjectLbl.text = travel.subject
            descLbl.text = travel.desc
            
        }
        if let food = food {
            
            photoImageView.image = food.photo
            ratingControl.rating = food.rating
            subjectLbl.text = food.subject
            descLbl.text = food.desc
            
        }
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    
        if segue.identifier == "ShowEdit" {
            
            let editEntry = segue.destinationViewController as! SpecificEditViewController
        
//            editEntry.subjectTxtFld.text = subjectLbl.text
//            editEntry.descTxtFld.text = descLbl.text
//            editEntry.photoImageView.image = photoImageView.image
//            editEntry.ratingControl.rating = ratingControl.rating
            editEntry.subject = subjectLbl.text!
            editEntry.desc = descLbl.text!
            editEntry.photo!.image = photoImageView.image
            editEntry.rating = ratingControl.rating
            
        }
        
    }
    
    @IBAction func unwindToSpecificList(sender: UIStoryboardSegue) {
        
        if let sourceViewController = sender.sourceViewController as? SpecificEditViewController {
            
            // Update entry.
            
            subjectLbl.text = sourceViewController.subjectTxtFld.text
            descLbl.text = sourceViewController.descTxtFld.text
            photoImageView.image = sourceViewController.photoImageView.image
            ratingControl.rating = sourceViewController.ratingControl.rating
        }
        
        
    }

    

}
