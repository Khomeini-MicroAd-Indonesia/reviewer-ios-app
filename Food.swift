//
//  Food.swift
//  reviewerApp
//
//  Created by Khomeini . on 7/7/16.
//  Copyright © 2016 Khomeini . All rights reserved.
//

import UIKit

class Food {
    
    // Mark: Properties
    
    var subject: String
    var desc: String
    var photo: UIImage?
    var rating: Int
    
    // Mark: Initialization
    
    init?(subject: String, desc: String, photo: UIImage?, rating: Int){
        
        // Initialized stored properties
        self.subject = subject
        self.desc = desc
        self.photo = photo
        self.rating = rating
        
        // Initialization should fail if there is no name or if the rating is negative.
        if subject.isEmpty || rating < 0 {
            return nil
        }
        
    }
    
}