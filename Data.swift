//
//  Data.swift
//  reviewerApp
//
//  Created by Khomeini . on 7/1/16.
//  Copyright © 2016 Khomeini . All rights reserved.
//

import Foundation

class Data {
    
    class Entry {
        
        let filename: String
        let heading: String
        
        init(fname: String, heading: String){
            
            self.heading = heading
            self.filename = fname
            
        }
        
    }
    
    let categories = [
        
        Entry(fname: "travel.jpeg", heading: "Travel"),
        Entry(fname: "food.jpeg", heading: "Food"),
        Entry(fname: "music.jpeg", heading: "Music"),
        Entry(fname: "movie.jpeg", heading: "Movie"),
        Entry(fname: "book.jpeg", heading: "Book")
        
    ]

    
}