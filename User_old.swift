//
//  User.swift
//  reviewerApp
//
//  Created by Khomeini . on 7/9/16.
//  Copyright © 2016 Khomeini . All rights reserved.
//

import UIKit

class User: NSObject, NSCoding {
    
    // Mark: Properties
    
    var username: String
    var email: String
    var password: String
    
    
    // MARK: Archiving Paths
    
    static let DocumentsDirectory = NSFileManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.URLByAppendingPathComponent("users")
    
    
    // MARK: Types
    
    struct PropertyKey {
        static let usernameKey = "username"
        static let emailKey = "email"
        static let passwordKey = "password"
    }
    
    // Mark: Initialization
    
    init?(username: String, email: String, password: String){
        
        // Initialized stored properties
        self.username = username
        self.email = email
        self.password = password
        
        super.init()
        
        // Initialization should fail if there is no name or if the rating is negative.
        if username.isEmpty && email.isEmpty && password.isEmpty {
            return nil
        }
        
    }
    
    // MARK: NSCoding
    
    func encodeWithCoder(aCoder: NSCoder) {
        
        aCoder.encodeObject(username, forKey: PropertyKey.usernameKey)
        aCoder.encodeObject(email, forKey: PropertyKey.emailKey)
        aCoder.encodeObject(password, forKey: PropertyKey.passwordKey)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let username = aDecoder.decodeObjectForKey(PropertyKey.usernameKey) as! String
        
        let email = aDecoder.decodeObjectForKey(PropertyKey.emailKey) as! String
        
        let password = aDecoder.decodeObjectForKey(PropertyKey.passwordKey) as! String
        
        
        self.init(username: username, email: email, password: password)
    }
    
    
}

