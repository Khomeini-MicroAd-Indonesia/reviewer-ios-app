//
//  SpecificTableViewCell.swift
//  reviewerApp
//
//  Created by Khomeini . on 7/5/16.
//  Copyright © 2016 Khomeini . All rights reserved.
//

import UIKit

class SpecificTableViewCell: UITableViewCell {
    
    // MARK: Properties
    @IBOutlet weak var subjectLabel: UILabel!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var ratingControl: RatingControl!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
}
