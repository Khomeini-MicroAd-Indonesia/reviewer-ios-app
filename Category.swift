//
//  Category.swift
//  reviewerApp
//
//  Created by Khomeini . on 7/7/16.
//  Copyright © 2016 Khomeini . All rights reserved.
//

import UIKit

class Category {
    
    // Mark: Properties
    
    var heading: String
    var photo: UIImage?
    
    // Mark: Initialization
    
    init?(heading: String, photo: UIImage?){
        
        // Initialized stored properties
        self.heading = heading
        self.photo = photo
        
    }
    
}
