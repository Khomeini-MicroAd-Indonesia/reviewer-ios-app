//
//  SpecificTableViewController.swift
//  reviewerApp
//
//  Created by Khomeini . on 7/5/16.
//  Copyright © 2016 Khomeini . All rights reserved.
//

import UIKit

class SpecificTableViewController: UITableViewController, UISearchBarDelegate{
    
    @IBOutlet var reviewListTbl: UITableView!
    @IBOutlet var searchBar: UISearchBar!
    
    // MARK: Properties
    var travels = [Travel]()
    var foods = [Food]()
    var stat:String = "Default"
    
//    let data = ["Slipknot", "Disturbed", "Deftones", "Killswitch Engage", "Incubus"]
//    
//    var filteredData: [String]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Use the edit button item provided by the table view controller.
        //navigationItem.leftBarButtonItem = editButtonItem()
        
        tableView.dataSource = self
        //searchBar.delegate = self
        
//        if stat == "music" {
//            filteredData = data
//        }
        
        loadSampleTravels()
        loadSampleFoods()
        
    }
    
    func loadSampleTravels() {
        
        let photo1 = UIImage(named: "Kinkakuji")!
        let travel1 = Travel(subject: "Kinkakuji Temple", desc: "Located in the ancient city of Kyoto", photo: photo1, rating: 4)!
        
        let photo2 = UIImage(named: "Himeji")!
        let travel2 = Travel(subject: "Himeji Castle", desc: "Located in Hyogo prefecture Japan", photo: photo2, rating: 5)!
        
        travels += [travel1, travel2]
        
    }
    
    func loadSampleFoods() {
        
        let photo1 = UIImage(named: "Spaghetti")!
        let food1 = Food(subject: "Spaghetti Meat Balls", desc: "With delicious beef meat balls", photo: photo1, rating: 4)!
        
        let photo2 = UIImage(named: "Chickensteak")!
        let food2 = Food(subject: "Black Pepper Chicken Steak", desc: "Dipped in delicious mushroom sauce", photo: photo2, rating: 5)!
        
        foods += [food1, food2]
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        return travels.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "SpecificTableViewCell"
        
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! SpecificTableViewCell

        // Configure the cell...
        
        // Fetches the appropriate data for the data source layout.
        
        if stat == "travel" {
            let travel = travels[indexPath.row]
            
            navigationItem.title = "Travel"
            cell.subjectLabel.text = travel.subject
            cell.photoImageView.image = travel.photo
            cell.ratingControl.rating = travel.rating
            
        }
        if stat == "food" {
            let food = foods[indexPath.row]
            
            navigationItem.title = "Food"
            cell.subjectLabel.text = food.subject
            cell.photoImageView.image = food.photo
            cell.ratingControl.rating = food.rating
            
        }
//        if stat == "music" {
//            let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as UITableViewCell!
//            cell.textLabel?.text = filteredData[indexPath.row]
//        }

        return cell
    }
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let row = indexPath.row
        print("Row: \(row)")
        
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if editingStyle == .Delete {
            // Delete the row from data source
            travels.removeAtIndex(indexPath.row)
            saveTravels()
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            
            
        }else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
        
    }

    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "SpecificDetail" {
            
            let specificDetailViewController = segue.destinationViewController as! SpecificDetailViewController
            
            // Get the cell that generated this segue.
            if let selectedSpecificCell = sender as? SpecificTableViewCell {
            
                let indexPath = tableView.indexPathForCell(selectedSpecificCell)!
                
                if stat == "travel"{
                    let selectedSpecific = travels[indexPath.row]
                    specificDetailViewController.travel = selectedSpecific
                }else if stat == "food" {
                    let selectedSpecific = foods[indexPath.row]
                    specificDetailViewController.food = selectedSpecific
                }
                
                
            }
            
        }
    }
 
    @IBAction func unwindToSpecificList(sender: UIStoryboardSegue) {
        
        if let sourceViewController = sender.sourceViewController as? SpecificViewController, travel = sourceViewController.travel {
//
//            // Add a new destination.
//            let newIndexPath = NSIndexPath(forRow: travels.count, inSection: 0)
//            
//            travels.append(travel)
//            
//            tableView.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: .Bottom)
//        }
        
        if let selectedIndexPath = tableView.indexPathForSelectedRow {
                // Update an existing travel.
                travels[selectedIndexPath.row] = travel
                tableView.reloadRowsAtIndexPaths([selectedIndexPath], withRowAnimation: .None)
            }
            else {
                // Add a new travel destination.
                let newIndexPath = NSIndexPath(forRow: travels.count, inSection: 0)
                travels.append(travel)
                tableView.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: .Bottom)
            }
            // Save the destination
            saveTravels()
        }
        
    }
    
    // MARK: NSCoding
    
    func saveTravels() {
        
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(travels, toFile: Travel.ArchiveURL.path!)
        
        if !isSuccessfulSave {
            print("Failed to save travel destination...")
        }
        
    }
    
    func loadTravels() -> [Travel]? {
        
        return NSKeyedUnarchiver.unarchiveObjectWithFile(Travel.ArchiveURL.path!) as? [Travel]
        
    }

}
