//
//  Travel.swift
//  reviewerApp
//
//  Created by Khomeini . on 7/4/16.
//  Copyright © 2016 Khomeini . All rights reserved.
//

import UIKit

class Travel: NSObject, NSCoding {
    
    // Mark: Properties
    
    var subject: String
    var desc: String
    var photo: UIImage?
    var rating: Int
    
    // MARK: Archiving Paths
    
    static let DocumentsDirectory = NSFileManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.URLByAppendingPathComponent("travels")
    
    
    // MARK: Types
    
    struct PropertyKey {
        static let subjectKey = "subject"
        static let descKey = "description"
        static let photoKey = "photo"
        static let ratingKey = "rating"
    }
    
    // Mark: Initialization
    
    init?(subject: String, desc: String, photo: UIImage?, rating: Int){
        
        // Initialized stored properties
        self.subject = subject
        self.desc = desc
        self.photo = photo
        self.rating = rating
        
        super.init()
        
        // Initialization should fail if there is no name or if the rating is negative.
        if subject.isEmpty || rating < 0 {
            return nil
        }
        
    }
    
    // MARK: NSCoding
    
    func encodeWithCoder(aCoder: NSCoder) {
        
        aCoder.encodeObject(subject, forKey: PropertyKey.subjectKey)
        aCoder.encodeObject(desc, forKey: PropertyKey.descKey)
        aCoder.encodeObject(photo, forKey: PropertyKey.photoKey)
        aCoder.encodeInteger(rating, forKey: PropertyKey.ratingKey)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let subject = aDecoder.decodeObjectForKey(PropertyKey.subjectKey) as! String
        let desc = aDecoder.decodeObjectForKey(PropertyKey.descKey) as! String
        
        // Because photo is an optional property of Travel, use conditional cast.
        let photo = aDecoder.decodeObjectForKey(PropertyKey.photoKey) as? UIImage
        let rating = aDecoder.decodeIntegerForKey(PropertyKey.ratingKey)
        
        
        self.init(subject: subject, desc: desc, photo: photo, rating: rating)
    }
    
    
    
    
}
