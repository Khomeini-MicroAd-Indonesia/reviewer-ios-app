//
//  SpecificViewController.swift
//  reviewerApp
//
//  Created by Khomeini . on 7/3/16.
//  Copyright © 2016 Khomeini . All rights reserved.
//

import UIKit

class SpecificViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    // MARK: - Properties
    
    @IBOutlet weak var subjectTxtFld: UITextField!
    @IBOutlet weak var descTxtFld: UITextField!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var ratingControl: RatingControl!
    @IBOutlet weak var saveBtn: UIBarButtonItem!
    @IBAction func cancelBtn(sender: AnyObject) {
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    var travel: Travel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Handle the text field's user input through delegate callbacks
        subjectTxtFld.delegate = self
        descTxtFld.delegate = self
        
        // Enable the Save button only if the text field has a valid Travel subject.
        checkValidSpecificSubject()
    }

    
    // MARK: - UITextFieldDelegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        // Hide the keyboard
        textField.resignFirstResponder()
        return true
        
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
        // Disable the Save button while editing.
        saveBtn.enabled = false
    
    }
    
    func checkValidSpecificSubject() {
        // Disable the Save button if the text field is empty.
        let text = subjectTxtFld.text ?? ""
        saveBtn.enabled = !text.isEmpty
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
       
        checkValidSpecificSubject()
        //navigationItem.title = textField.text
        
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        // Dismiss the picker if the user canceled.
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        // The info dictionary contains multiple representations of the image, and this uses the original.
        let selectedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        // Set photoImageView to display the selected image.
        photoImageView.image = selectedImage
        
        // Dismiss the picker.
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    // This method lets you configure a view controller before it's presented.
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if saveBtn === sender {
            let subject = subjectTxtFld.text ?? ""
            let desc = descTxtFld.text ?? ""
            let photo = photoImageView.image
            let rating = ratingControl.rating
            
            // Set the destination to be passed to SpecificTableViewController after the unwind segue.
            travel = Travel(subject: subject, desc: desc, photo: photo, rating: rating)
            
            
        }
        
    }
    
    // MARK: - Action
    
    
    @IBAction func selectImageFromPhotoLibrary(sender: UITapGestureRecognizer) {
        
        // Hide the keyboard.
        subjectTxtFld.resignFirstResponder()
        descTxtFld.resignFirstResponder()
        
        // UIImagePickerController is a view controller that lets a user pick media from their photo library.
        let imagePickerController = UIImagePickerController()
        
        // Only allow photos to be picked, not taken.
        imagePickerController.sourceType = .PhotoLibrary
        
        // Make sure ViewController is notified when the user picks an image.
        imagePickerController.delegate = self
        
        presentViewController(imagePickerController, animated: true, completion: nil)
        
    }
    
    

}
