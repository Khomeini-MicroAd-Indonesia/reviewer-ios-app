//
//  SignInViewController.swift
//  reviewerApp
//
//  Created by Khomeini . on 7/7/16.
//  Copyright © 2016 Khomeini . All rights reserved.
//

import UIKit
import CoreData

class SignInViewController: UIViewController {
    
    // MARK: Properties
    
    @IBOutlet weak var emailTxtFld: UITextField!
    @IBOutlet weak var passwordTxtFld: UITextField!
    
    var user = [Users]()
    var message:String = ""
    
    // MARK: Action
    
    @IBAction func okBtn(sender: AnyObject) {
        
        fetch()
        
    }
    
    @IBAction func cancelBtn(sender: AnyObject) {
        
        emailTxtFld.text = ""
        passwordTxtFld.text = ""
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //loadUsers()
        
        // Load any saved meals, otherwise load sample data.
//        if let savedUsers = loadUsers() {
//            users += savedUsers
//        }else {
//            // Load the sample data.
//            loadSampleUsers()
//        }
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func unwindToUserList(sender: UIStoryboardSegue) {
//        if let sourceViewController = sender.sourceViewController as? SignUpViewController, user = sourceViewController.user {
//            
//            // Add a new user.
//            users.append(user)
//            
//        }
    }
    
//    func loadUsers() -> [User]? {
//        
//        return NSKeyedUnarchiver.unarchiveObjectWithFile(User.ArchiveURL.path!) as? [AAAUserMO]
//    }
    
//    func fetch() {
//        
//        
//        let moc = DataController().managedObjectContext
//        
//        let userFetch = NSFetchRequest()
//        
//        let entityDescription =  NSEntityDescription.entityForName("Users", inManagedObjectContext: moc)
//        
//        userFetch.entity = entityDescription
//        
//        do {
//            let fetchedUser = try moc.executeFetchRequest(userFetch) //as! [Users]
//            
//           print(fetchedUser)
//            
//            
//        } catch {
//            fatalError("Failed to fetch usr: \(error)")
//        }
//        
//        
//
//        
//    }
    
    func alertMessage (message:String){
        
        let alertController = UIAlertController(title: "System Message", message:
            message, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
        self.presentViewController(alertController, animated: true, completion: nil)
        
    }
    
    
    func fetch() {
        
        let appDelegate =
            UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        //2
        let fetchRequest = NSFetchRequest(entityName: "Users")
        
        let predicate = NSPredicate(format: "email == %@ AND password == %@", emailTxtFld.text!, passwordTxtFld.text!)
        fetchRequest.predicate = predicate
        //3
        do {
            let results =
                try managedContext.executeFetchRequest(fetchRequest)
            user = results as! [Users]
            
            let row = user.count
            
            if row > 0 {
                
                message = "You're signed in"
                //self.performSegueWithIdentifier("signInIdentifier", sender: self)
            }else{
                
                message = "Email & password combination is incorrect!"
            }
            
            alertMessage(message)
            
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
        
    }
    
}
