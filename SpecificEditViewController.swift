//
//  SpecificEditViewController.swift
//  reviewerApp
//
//  Created by Khomeini . on 7/8/16.
//  Copyright © 2016 Khomeini . All rights reserved.
//

import UIKit

class SpecificEditViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    // MARK: Properties
    
    @IBOutlet weak var subjectTxtFld: UITextField!
    @IBOutlet weak var descTxtFld: UITextField!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var ratingControl: RatingControl!
    @IBOutlet weak var saveBtn: UIBarButtonItem!
//    @IBAction func cancelBtn(sender: AnyObject) {
//        dismissViewControllerAnimated(true, completion: nil)
//    }
    
    var subject:String?
    var desc:String?
    let photo: UIImageView? = UIImageView.init(image: nil)
    var rating:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        subjectTxtFld.text = subject
        descTxtFld.text = desc
        photoImageView.image = photo?.image
        ratingControl.rating = rating
    }
    
    // MARK: - UITextFieldDelegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        // Hide the keyboard
        textField.resignFirstResponder()
        return true
        
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
        // Disable the Save button while editing.
        saveBtn.enabled = false
        
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        
        //checkValidSpecificSubject()
        //navigationItem.title = textField.text
        
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        // Dismiss the picker if the user canceled.
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        // The info dictionary contains multiple representations of the image, and this uses the original.
        let selectedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        // Set photoImageView to display the selected image.
        photoImageView.image = selectedImage
        
        // Dismiss the picker.
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    // MARK: - Action
    
    
    @IBAction func selectImageFromPhotoLibrary(sender: UITapGestureRecognizer) {
        
        // Hide the keyboard.
        subjectTxtFld.resignFirstResponder()
        descTxtFld.resignFirstResponder()
        
        // UIImagePickerController is a view controller that lets a user pick media from their photo library.
        let imagePickerController = UIImagePickerController()
        
        // Only allow photos to be picked, not taken.
        imagePickerController.sourceType = .PhotoLibrary
        
        // Make sure ViewController is notified when the user picks an image.
        imagePickerController.delegate = self
        
        presentViewController(imagePickerController, animated: true, completion: nil)
    }

}
