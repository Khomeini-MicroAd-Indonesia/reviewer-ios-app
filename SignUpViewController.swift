//
//  SignUpViewController.swift
//  reviewerApp
//
//  Created by Khomeini . on 7/7/16.
//  Copyright © 2016 Khomeini . All rights reserved.
//

import UIKit
import CoreData

class SignUpViewController: UIViewController, UITextFieldDelegate {
    
    var message:String = ""
    
    // MARK: Properties
    @IBOutlet var usernameTxtFld: UITextField!
    @IBOutlet var emailTxtFld: UITextField!
    @IBOutlet var passwordTxtFld: UITextField!
    @IBOutlet var confirmationTxtFld: UITextField!
    @IBAction func registerUser(sender: AnyObject) {
        
//        let username = usernameTxtFld.text
//        let email = emailTxtFld.text
        let password = passwordTxtFld.text
        let confirm = confirmationTxtFld.text
        
        if password != confirm {
            
            message = "Passwords don't match!"
            alertMessage(message)
        }
        
        saveUser()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Handle the text field's user input through delegate callbacks
        usernameTxtFld.delegate = self
        emailTxtFld.delegate = self
        passwordTxtFld.delegate = self
        confirmationTxtFld.delegate = self
        
        // Enable the Save button only if the text field has a valid Travel subject.
        checkValidSpecificSubject()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UITextFieldDelegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        // Hide the keyboard
        textField.resignFirstResponder()
        return true
        
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
        // Disable the Save button while editing.
        //registerUser.enabled = false
        
    }
    
    func checkValidSpecificSubject() {
        // Disable the Save button if the text field is empty.
        //let emailTxt = emailTxtFld.text ?? ""
        
        //registerUser.enabled = !emailTxt.isEmpty
        
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        
        checkValidSpecificSubject()
        
    }
    
    func alertMessage (message:String){
        
        let alertController = UIAlertController(title: "System Message", message:
            message, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
        self.presentViewController(alertController, animated: true, completion: nil)
        
    }
    
    // MARK: Action
    
    @IBAction func cancelBtn(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    

    // MARK: NSCoding
    
        
    func saveUser() {
        
        let appDelegate =
            UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        //2
        let entity = NSEntityDescription.insertNewObjectForEntityForName("Users", inManagedObjectContext: managedContext) as! Users
        
        
        //3
        entity.setValue(usernameTxtFld.text, forKey: "username")
        entity.setValue(emailTxtFld.text, forKey: "email")
        entity.setValue(passwordTxtFld.text, forKey: "password")
        
        //4
        do {
            try managedContext.save()
            //5
            message = "Signing up is successful"
            alertMessage(message)
            
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
        
        
    }

}
