//
//  Users+CoreDataProperties.swift
//  reviewerApp
//
//  Created by Khomeini . on 7/10/16.
//  Copyright © 2016 Khomeini . All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Users {

    @NSManaged var username: String?
    @NSManaged var email: String?
    @NSManaged var password: String?

}
