//
//  CategoriesTableViewController.swift
//  reviewerApp
//
//  Created by Khomeini . on 7/1/16.
//  Copyright © 2016 Khomeini . All rights reserved.
//

import UIKit

class CategoriesTableViewController: UITableViewController {
    
    // MARK: Properties
    
    //let data = Data()
    var categories = [Category]()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.tableView.rowHeight = 300.0
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        
        // Load category samples
        loadSampleCategories()
    
    }
    
    func loadSampleCategories() {
        
        let photo1 = UIImage(named: "Travel")!
        let category1 = Category(heading: "TRAVEL", photo: photo1)!
        
        let photo2 = UIImage(named: "Food")!
        let category2 = Category(heading: "FOOD", photo: photo2)!
        
        let photo3 = UIImage(named: "Music")!
        let category3 = Category(heading: "MUSIC", photo: photo3)!
        
        let photo4 = UIImage(named: "Movie")!
        let category4 = Category(heading: "MOVIE", photo: photo4)!
        
        let photo5 = UIImage(named: "Book")!
        let category5 = Category(heading: "BOOK", photo: photo5)!
        
        categories += [category1, category2, category3, category4, category5]
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        //return data.categories.count
        return categories.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! CategoriesTableViewCell
        
        let category = categories[indexPath.row]
        
        cell.headingLabel.text = category.heading
        cell.bgImageView.image = category.photo
     
        return cell
     }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let row = indexPath.row
        print("We clicked row: \(row)")
        
        
    }
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "CategoryDetail" {
            
            let specificTableViewController = segue.destinationViewController as! SpecificTableViewController
            
            
            // Get the cell that generated this segue.
            if let selectedCategoryCell = sender as? CategoriesTableViewCell {
                
                let indexPath = tableView.indexPathForCell(selectedCategoryCell)!
                //print(indexPath.row)
                //let selectedCategory = categories[indexPath.row]
                
                switch indexPath.row {
                case 0:
                    specificTableViewController.stat = "travel"
                    break
                case 1:
                    specificTableViewController.stat = "food"
                    break
                case 2:
                    specificTableViewController.stat = "music"
                    break
                case 3:
                    specificTableViewController.stat = "movie"
                    break
                case 4:
                    specificTableViewController.stat = "book"
                    break
                default: break
                    
                }

                
            }
            
        }
    }



}
